import { Component } from '@angular/core';
import { URLS } from '../objects/urls';

@Component({
    moduleId: module.id,
    selector: 'index',
    template: `<div>
                <h2>This is home page</h2>
                <chatroom-new></chatroom-new>
               </div>
            `,
    styles: [`
                
            `]
})
export class IndexComponent {
    
}