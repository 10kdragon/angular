import { Component, Input } from '@angular/core';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'message',
  template: `
            <p-messages [value]='msgs'></p-messages>
            `,
    styles: [`
            
            `],
})
export class MessageComponent {
    @Input('msgs')
    msgs: Message[] = [];
}


