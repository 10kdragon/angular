import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { MessageComponent } from './message.component';
import { Message } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { URLS } from '../objects/urls';
import { ChatroomService } from '../services/chatroom.service';

import { ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
    moduleId: module.id,
    selector: 'chatroom-ui',
    template: `
                <div class="container">
                <message [msgs]="msgs"></message>
                <div *ngIf="!submitted">
                    <h2>Look</h2>
                    <div class='video-container'>
                        <div class='remote-div'>
                            <h3>{{remoteState}}</h3>
                            <div class='video-div'>                            
                                <video width=450px height=330px autoplay id='remoteView'></video>
                            </div>
                        </div>
                        <div class='self-div'>
                            <h3>{{selfState}}</h3>
                            <div class='video-div'>
                                <video width=450px height=330px  autoplay id='selfView'></video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `,
    styles: [`
                .video-container {
                    display: block;
                }
                .remote-div {
                    display: inline-block;
                    margin: 20px 30px;
                }
                .self-div {
                    display: inline-block;
                    margin: 20px 30px;
                }
                .video-div {
                    border: 1px solid grey;
                    background-color: grey;
                }
            `]
})
export class ChatroomUiComponent implements OnInit {
    chatroomName: string;
    code: string;
    nonce: string;
    isCaller: boolean;
    chatroomPrivate: string;
    selfView: any;
    remoteView: any;
    selfState: string = 'Self View';
    remoteState: string = 'Waiting for participant';

    // webrtc
    ws: any // websocket
    publicChannel: string = 'lobby';
    pc: any;
    peerConnectionConfig = {'iceServers': [{'url': 'stun:stun.services.mozilla.com'}, {'url': 'stun:stun.l.google.com:19302'}]};

    navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
    window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

    constructor(private route: ActivatedRoute) {
        this.route.params
            .subscribe((params: Params) => this.chatroomName = params['chatroom_name']);
        // console.log(this.chatroomName);
        this.code = sessionStorage.getItem('chatroom_code');


        this.prepareWebRTC(); // add local stream should happen before setLocalDescription

        // websocket
        this.ws = new WebSocketRails(URLS.url_ws);
        this.ws.on_open = (data: any) => {
            console.log('websocket opened.');
            // public channel only used to send in data; chatroom for receive data before getting role
            this.ws.bind(this.publicChannel, (data: any) => {console.log(this.publicChannel + JSON.stringify(data))});
            this.ws.bind(this.chatroomName, (data: any) => this.onChatroomMessage(data));
            
            // notify ready
            this.nonce = this.nonce128();
            this.ws.trigger(this.publicChannel, {'ready': {'chatroom': this.chatroomName, 'code': this.code}, 'nonce': this.nonce});

        }

        this.ws.on_close = (data: any) => { console.log("WS closing."); console.log(data); };
    }

    onChatroomMessage(data: any): void {
        console.log(this.chatroomName + JSON.stringify(data));

        if (data.role && data.nonce == this.nonce) {
            this.nonce = this.nonce128(); // update nonce
            console.log("update role.");
            if (data.role == 'caller') {
                this.isCaller = true;
                // this.chatroomPrivate = this.chatroomName + '-caller';
                // this.ws.bind(this.chatroomPrivate, (data: any) => this.onChatroomPrivateMessage(data));
            }
            else {
                this.isCaller = false;
                // this.chatroomPrivate = this.chatroomName + '-callee';
                // this.ws.bind(this.chatroomPrivate, (data: any) => this.onChatroomPrivateMessage(data));
            }

        }
        if (data.state && data.state == 'connected') {
            console.log('!!Starting RTC.');
            this.startWebRTC();
        }
        if (data.ice) {
            if ((data.role == 'caller') != this.isCaller) {
                // if (data.ice != null) // could be null
                    this.pc.addIceCandidate(new RTCIceCandidate(data.ice));
            }
        }
        if (data.sdp) {
            if ((data.role == 'caller') != this.isCaller) {
                this.pc.setRemoteDescription(new RTCSessionDescription(data.sdp));
                if (!this.isCaller) {
                    console.log('!!Starting RTC.');
                    this.startWebRTC();
                }
            }
        }
    }

    // // message receive for caller/calle only
    // onChatroomPrivateMessage(data: any): void {
    //     console.log(data);
    //     console.log('PRIVATE: ' + JSON.stringify(data));
    //     // data = JSON.parse(data);

    //     if (data.state && data.state == 'connected') {
    //         console.log('!!Starting RTC.');
    //         this.startWebRTC();
    //     }
    //     if (data.ice) {
    //         if (data.ice != null) // could be null
    //             this.pc.addIceCandidate(new RTCIceCandidate(data.ice));
    //     }
    //     if (data.sdp) {
    //         this.pc.setRemoteDescription(new RTCSessionDescription(data.sdp));
    //     }
    // }

    prepareWebRTC(): void {
        this.pc = new window.RTCPeerConnection(this.peerConnectionConfig);
        this.pc.onicecandidate = (evt: any) => {
            this.gotIceCandidate(evt);    
        };

        this.pc.onaddstream = (evt: any)=> {
            this.onAddStream(evt);
        }
        
        let vgaConstraints = {
            video: {width: {exact: 640}, height: {exact: 480}},
            audio: true
        };

        let hdConstraints = {
            video: {width: {exact: 1280}, height: {exact: 720}},
            audio: true
        };

        navigator.getUserMedia({ "audio": true, "video": true },
            (stream: any)=>{
                this.pc.addStream(stream);
                document.getElementById('selfView').src = URL.createObjectURL(stream);
            },
            ()=>{alert('getUserMedia not supported.');}
        );
    }

    startWebRTC(): void {
        if (this.isCaller) {
            console.log('creating offer.');
            this.pc.createOffer((desc: any)=>this.gotDescription(desc), (err: any)=>console.log(err));
        }
        else {
            console.log('creating answer.');
            this.pc.createAnswer((desc: any)=>this.gotDescription(desc), (err: any)=>console.log(err));
        }
    }

    onAddStream(evt: any): void {
        console.log('remote stream added');
        this.remoteState = 'Connected';
        document.getElementById('remoteView').src = URL.createObjectURL(evt.stream);
    }

    gotIceCandidate(evt: any): void {
        console.log('got ice');
        this.ws.trigger(this.publicChannel, { 'ice': evt.candidate, 'chatroom': this.chatroomName, 'code': this.code });
    }

    gotDescription(desc: any): void {
        console.log('got descriptoin');
        this.pc.setLocalDescription(desc);
        this.ws.trigger(this.publicChannel, { 'sdp': desc, 'chatroom': this.chatroomName, 'code': this.code });
    }

    nonce128(): string {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i = 0; i < 128; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    ngOnInit(): void {

    }
}