import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { MessageComponent } from './message.component';
import { Message } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { URLS } from '../objects/urls';
import { ChatroomService } from '../services/chatroom.service';

@Component({
    moduleId: module.id,
    selector: 'chatroom-new',
    template: `
                <div class="container">
                <message [msgs]="msgs"></message>
                <div *ngIf="!submitted">
                    <h2></h2>
                    <form [formGroup]="chatroomNewForm" (ngSubmit)="onSubmit()">
                        <div class="form-group">
                            <h3 class="first">Create a room to chat</h3>
                            <input id="chatroomName" type="text" size="30" pInputText required formControlName="chatroomName" />
                            <span class="alert alert-danger" *ngIf="formErrors['chatroomName']">{{formErrors['chatroomName']}}</span>
                        </div>
                        <br/>
                        <div class="form-group">
                            <button type="submit" pButton type="text" (click)="onSubmit()" [disabled]="chatroomNewForm.invalid || formDisabled" label="Start Chat"></button>
                        </div>
                    </form>
                </div>
            </div>
            `,
    styles: [`
                .ng-invalid[required].ng-touched {
                    border-bottom: 5px solid red;
                }
                .ng-valid[required].ng-touched {
                    border-bottom: 5px solid green;
                }
                .alert-danger {
                    color: red;
                }
            `]
})
export class ChatroomNewComponent implements OnInit {
    ws: any; // websocket
    chatroomNewForm: FormGroup;
    chatroomInfo: ChatroomInfo = new ChatroomInfo;
    formDisabled : boolean;
    
    msgs: Message[] = [];

    formErrors = {
        'chatroomName': '',
    };

    validationMessages = {
        'chatroomName': {
            'required': 'Chatroom Name is required. ',
            // 'email-format': 'Invalid Email address. ',
            'minlength': 'Name is too short',
            'maxlength': 'Name is too long '
        },
    };

    constructor(private fb: FormBuilder, 
                private userService: UserService, 
                private chatroomService: ChatroomService) {
        // this.ws = new WebSocketRails(URLS.url_ws);
        // this.ws.on_open = (data: any) => {
        //     let channel: string = 'chatroom';
        //     this.ws.bind(channel, (data: any) => {console.log(channel + JSON.stringify(data))});
        // }
    }

    ngOnInit(): void {
        // build form
        this.chatroomNewForm = this.fb.group({
            'chatroomName': [this.chatroomInfo.chatroomName, [
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(255),
                ]
            ],
        });

        this.chatroomNewForm.valueChanges.subscribe(data => this.onFormValueChanged(data));
    }

    onFormValueChanged(data?: any) {
        const form = this.chatroomNewForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.touched && !control.valid) {
                for (let key in control.errors) {
                    this.formErrors[field] += this.validationMessages[field][key];
                }
            }
        }
    }
    
    onSubmit(): void {
        this.msgs = [];
        this.formDisabled = true;
        this.chatroomInfo = this.chatroomNewForm.value;
        // this.ws.trigger('chatroom', this.chatroomInfo);
        this.chatroomService.createChatroom(this.chatroomInfo.chatroomName)
            .then((res) => {
                if(res['status'] == 'ok') {
                    sessionStorage.setItem('chatroom_code', res['chatroom_code']);
                    // change location
                    window.location.href = '/chat/' + this.chatroomInfo.chatroomName;
                }
                else if(res['status'] == 'existed') {
                    this.msgs.push({severity:'error', summary:'Try Again', detail:'The chatroom name is already taken.'});   
                    this.formDisabled = false;
                }
                else {
                    this.msgs.push({severity:'error', summary:'Try Again Later', detail:'Something went wrong. Try again.'});                       
                    this.formDisabled = false;
                }
            })
            .catch((res) => {
                this.msgs.push({severity:'error', summary:'Try Again Later', detail:'Something went wrong. Try again.'});                       
                console.log(res);
            });
    }

}

class ChatroomInfo {
    chatroomName: string;
}