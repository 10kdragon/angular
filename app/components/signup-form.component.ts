import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Message } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegexValidator, COMMON_REGEX } from '../directives/regex-validator.directive';

@Component({
    moduleId: module.id,
    selector: 'signup-form',
    templateUrl: 'signup-form.component.html',
    styles: [`
                .ng-invalid[required].ng-touched {
                    border-bottom: 5px solid red;
                }
                .ng-valid[required].ng-touched {
                    border-bottom: 5px solid green;
                }
                .alert-danger {
                    color: red;
                }
            `]
})
export class SignupFormComponent implements OnInit {
    signupForm: FormGroup;
    signupInfo: SignupInfo = new SignupInfo;
    signupResponse: SignupResponse;
    msgs: Message[] = [];
    submitted = false;

    formErrors = {
        'email': '',
        'name': '',
        'password': '',
        'password_confirmation': ''
    };

    validationMessages = {
        'email': {
            'required': 'Email is required. ',
            'email-format': 'Invalid Email address. ',
        },
        'name': {
            'required': 'Name is required. ',
            'minlength': 'Name is too short. ',
            'maxlength': 'Name is too long. '
        },
        'password': {
            'required': 'Password is required. ',
            'minlength': 'Password is too short. ',
            'maxlength': 'Pasword is too long. '
        },
        'password_confirmation': {
            'required': 'Password confirmation is required. ',
            'mismatch': 'password confirmation does not match password. '
        },
    };

    constructor(private fb: FormBuilder, private userService: UserService) {
    
    }

    ngOnInit() {
        // build form
        this.signupForm = this.fb.group({
            'email': [this.signupInfo.email, [
                Validators.required,
                RegexValidator(COMMON_REGEX.email, 'email-format'),
                ]
            ],
            'name': [this.signupInfo.name, [
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(255),
                ]
            ],
            'password': [this.signupInfo.password, [
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(255)
                ]
            ],
            'password_confirmation': [this.signupInfo.password_confirmation, [
                    Validators.required,
                ]
            ]
        });

        this.signupForm.valueChanges.subscribe(data => this.onFormValueChanged(data));
    }

    onFormValueChanged(data?: any) {
        const form = this.signupForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.touched && !control.valid) {
                // console.log(control.errors.toString());
                for (let key in control.errors) {
                    // console.log('field:'+field);
                    // console.log('key:'+key);
                    this.formErrors[field] += this.validationMessages[field][key];
                }
            }
        }
    }

    onSubmit(): void {
        this.signupInfo = this.signupForm.value;
        this.userService.signup(this.signupInfo.email, this.signupInfo.name, 
                    this.signupInfo.password, this.signupInfo.password_confirmation)
        .then(res => {
            this.signupResponse = res;
            if (this.signupResponse.status == 'ok') {
                localStorage.setItem('token1', this.signupResponse.token1);
                localStorage.setItem('token2', this.signupResponse.token2);
                this.msgs.push({severity:'success', summary:'Congratulations', detail:'You have signed up!'});   

                // announce login
                this.userService.announceUserLoginStatusChange(true);

                this.submitted = true;
            }
            else {
                let errorMsg: string = '';
                if (this.signupResponse.errors) {
                    for (const key in this.signupResponse.errors) {
                        errorMsg += (key + ': ' + this.signupResponse.errors[key].join(', ') + '! ');
                    }
                }
                this.msgs.push({severity:'error', summary:'Sign Up Failed', detail: errorMsg + 'Please try again.'});                   
                this.submitted = false;
            }
        }).catch(res => {
            this.msgs.push({severity:'error', summary:'Error', detail:'Something went wrong..'});               
            this.submitted = false;
        });

        this.submitted = true;
        this.msgs = [];
        event.preventDefault();
    }
}

export class SignupInfo {
    email: string;
    name: string
    password: string;
    password_confirmation: string;
    constructor() {
        this.email = '';
        this.name = '';
        this.password = '';
        this.password_confirmation = '';
    }
}

interface SignupResponse {
    status: string;
    token1?: string;
    token2?: string;
    errors?: any;
}