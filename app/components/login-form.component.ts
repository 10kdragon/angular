import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { MessageComponent } from './message.component';
import { Message } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegexValidator, COMMON_REGEX } from '../directives/regex-validator.directive';

@Component({
    moduleId: module.id,
    selector: 'login-form',
    templateUrl: 'login-form.component.html',
    styles: [`
                .ng-invalid[required].ng-touched {
                    border-bottom: 5px solid red;
                }
                .ng-valid[required].ng-touched {
                    border-bottom: 5px solid green;
                }
                .alert-danger {
                    color: red;
                }
            `]
})
export class LoginFormComponent implements OnInit {
    loginForm: FormGroup;
    loginInfo: LoginInfo = new LoginInfo;
    loginResponse: LoginResponse;
    msgs: Message[] = [];
    submitted = false;

    formErrors = {
        'email': '',
        'password': ''
    };

    validationMessages = {
        'email': {
            'required': 'Email is required. ',
            'email-format': 'Invalid Email address. '
        },
        'password': {
            'required': 'Password is required ',
            'minlength': 'Invalid password length ',
            'maxlength': 'Invalid password length '
        },
    };

    constructor(private fb: FormBuilder, private userService: UserService) {
    
    }

    ngOnInit() {
        // build form
        this.loginForm = this.fb.group({
            'email': [this.loginInfo.email, [
                Validators.required,
                RegexValidator(COMMON_REGEX.email, 'email-format'),
                ]
            ],
            'password': [this.loginInfo.password, [
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(255)
                ]
            ]
        });

        this.loginForm.valueChanges.subscribe(data => this.onFormValueChanged(data));
    }

    onFormValueChanged(data?: any) {
        const form = this.loginForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.touched && !control.valid) {
                for (let key in control.errors) {
                    this.formErrors[field] += this.validationMessages[field][key];
                }
            }
        }
    }

    onSubmit(): void {
        this.loginInfo = this.loginForm.value;
        this.userService.login(this.loginInfo.email, this.loginInfo.password)
            .then((res) => {
                this.loginResponse = res;
                if (this.loginResponse.status == 'ok') {
                    localStorage.setItem("token1", this.loginResponse.token1);       
                    localStorage.setItem("token2", this.loginResponse.token2); 
                    
                    // announce login
                    this.userService.announceUserLoginStatusChange(true);

                    this.msgs.push({severity:'success', summary:'Login Successful', detail:'Welcome!'});   
                }
                else {
                    this.msgs.push({severity:'error', summary:'Login Failed', detail:'Invalid email or password.'});   
                    this.loginForm.get('password').setValue('');
                    this.submitted = false;
                }
        }).catch((res) => {
            this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Something went wrong.' });   
            this.loginForm.get('password').setValue('');
            this.submitted = false;
        });

        this.submitted = true;
        this.msgs = [];
        event.preventDefault();
    }
}

export class LoginInfo {
    email: string;
    password: string;
    constructor() {
        this.email = '';
        this.password = '';
    }
}

interface LoginResponse {
    status: string;
    token1?: string;
    token2?: string;
}