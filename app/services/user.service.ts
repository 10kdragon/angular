import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Subject }    from 'rxjs/Subject';
import { URLS } from '../objects/urls';

@Injectable()
export class UserService {
    private userLoginStatusSource = new Subject<boolean>();
    userLoginStatusChanged$ = this.userLoginStatusSource.asObservable();

    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {

    }

    announceUserLoginStatusChange(loggedIn: boolean) {
        this.userLoginStatusSource.next(loggedIn);
    }

    login(email: string, password: string): Promise<any> {
        // return Promise.resolve({status: 'fine'});
        let params = {};
        params['user'] = {'email': email, 'password': password};
        // console.log(JSON.stringify(params));
        return this.http.post(URLS.url_login, JSON.stringify(params), {headers: this.headers})
        .toPromise()
        .then((res) => res.json())
        .catch((res) => console.log(res));
    }

    signup(email: string, name: string, password: string, password_confirmation: string): Promise<any> {
        let params = {};
        params['user'] = {'email': email, 'name': name, 'password': password, 'password_confirmation': password_confirmation};
        //return Promise.resolve(JSON.stringify(params));
        return this.http.post(URLS.url_signup, JSON.stringify(params), {headers: this.headers})
        .toPromise()
        .then((res) => res.json())
        .catch((res) => console.log(res));
    }
}