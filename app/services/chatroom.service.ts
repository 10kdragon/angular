import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { URLS } from '../objects/urls';

@Injectable()
export class ChatroomService {
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {

    }

    createChatroom(name: string): Promise<any> {
        let params = {};
        return this.http.post(URLS.url_new_chatroom + name, JSON.stringify(params), {headers: this.headers})
        .toPromise()
        .then((res) => res.json())
        .catch((res) => console.log(res));
    }

    joinChatroom(name: string): Promise<any> {
        return this.http.get(URLS.url_new_chatroom + name, {headers: this.headers})
        .toPromise()
        .then((res) => res.json())
        .catch((res) => console.log(res));
    }
}