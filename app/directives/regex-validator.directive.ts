import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

@Directive({
    selector: '[regexValidator]',
    providers: [{provide: NG_VALIDATORS, 
        useExisting: RegexValidatorDirective, 
        multi: true}]
})
export class RegexValidatorDirective {
    @Input() regPattern: string;
    @Input() key: string;
    @Input() message: string;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['regPattern'];
        if (change) {
            const val: RegExp = change.currentValue;
            const re = new RegExp(val, 'i');
            this.valFn = RegexValidator(re, this.key);
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}

export function RegexValidator(re: RegExp, validationMessageKey: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const val = control.value;
        const good = re.test(val);
        
        if (good) {
            return null;
        }
        else {
            let t: {[key: string] : any} = {};
            t[validationMessageKey] = '';
            return t;
        }
    };
}

export const COMMON_REGEX = {
    'email' : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    
}