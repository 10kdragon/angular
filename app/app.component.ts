import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'my-app',
  template: `
              <div class='top-nav'>
                <ul>
                  <li class='dropdown'>
                    <a href="javascript:void(0)" class="dropbtn">Account</a>
                    <div class='dropdown-content'>
                      <a *ngIf='!loggedIn' routerLink="/signup">Sign Up</a>
                      <a *ngIf='!loggedIn' routerLink="/login">Log In</a>
                      <a *ngIf='loggedIn' href='/login' (click)='logout()'>Log Out</a>
                    </div>
                  </li>
                  <li>
                    <a routerLink="/">Home</a>
                  </li>
                </ul>
              </div>
              <div class='page-content'>
                <router-outlet></router-outlet>
              <div>
            `,
})
export class AppComponent {
  loggedIn: boolean = false;

  constructor(private userService: UserService) {
    userService.userLoginStatusChanged$
      .subscribe((loggedIn) => {this.loggedIn = loggedIn});
  }

  logout(): void {
    localStorage.clear();
    this.userService.announceUserLoginStatusChange(false);
  }
}

