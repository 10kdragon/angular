import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { AuthComponent } from './components/auth.component';
import { MessageComponent } from './components/message.component';
import { IndexComponent } from './components/index.component';
import { LoginFormComponent } from './components/login-form.component';
import { SignupFormComponent } from './components/signup-form.component';
import { ChatroomNewComponent } from './components/chatroom-new.component';
import { ChatroomUiComponent } from './components/chatroom-ui.component';

import { UserService } from './services/user.service';
import { ChatroomService } from './services/chatroom.service';

import {MessagesModule} from 'primeng/primeng';
import {InputTextModule} from 'primeng/primeng';
import {PasswordModule} from 'primeng/primeng';
import {ButtonModule} from 'primeng/primeng';

@NgModule({
  imports:      [
                  BrowserModule,
                  FormsModule, 
                  ReactiveFormsModule,
                  RouterModule.forRoot([
                    {
                      path: '',
                      redirectTo: '/index',
                      pathMatch: 'full'
                    },
                    {
                      path: 'index',
                      component: IndexComponent
                    },
                    {
                      path: 'login',
                      component: LoginFormComponent
                    },
                    {
                      path: 'signup',
                      component: SignupFormComponent
                    },
                    {
                      path: 'chat/:chatroom_name',
                      component: ChatroomUiComponent
                    }
                  ]),
                  HttpModule,
                  // primeNg modules
                  MessagesModule,
                  InputTextModule,
                  PasswordModule,
                  ButtonModule,
                ],
  declarations: [ 
                  AppComponent,
                  AuthComponent,
                  MessageComponent,
                  IndexComponent,
                  LoginFormComponent,
                  SignupFormComponent,
                  ChatroomNewComponent,
                  ChatroomUiComponent,
                ],
  providers:    [
                  UserService,
                  ChatroomService,
                ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
