export const URLS = {
    // url_login: 'https://murmuring-plains-14121.herokuapp.com/log_in',
    // url_signup: 'https://murmuring-plains-14121.herokuapp.com/sign_up',
    // url_new_chatroom: 'https://murmuring-plains-14121.herokuapp.com/chatrooms/new',
    // url_join_chatroom: 'http://murmuring-plains-14121.herokuapp.com/chatrooms/',    
    // url_ws: 'murmuring-plains-14121.herokuapp.com/websocket',
    url_login: 'http://localhost:3000/log_in',
    url_signup: 'http://localhost:3000/sign_up',
    url_new_chatroom: 'http://localhost:3000/chatrooms/new/',
    url_join_chatroom: 'http://localhost:3000/chatrooms/',
    url_ws: 'localhost:3000/websocket',
};